include("tfidf.jl")
using CSV, DataFrames
@time begin
    data = CSV.read("datasets/test.csv")
    descr = CSV.read("datasets/product_descriptions.csv")
    data = join(data, descr, on=:product_uid)
    unique!(data, :product_uid)
end

arguments = ARGS
k = 10
filename = "report/table_big.tex"

@time begin
    matrix = tf_idf(join(arguments, " "),  data.product_description)
    result = DataFrame(matrix)
    rename!(result, Symbol.(arguments))
    result.sum = sum(eachcol(result))
    result.product = data.product_title
    sort!(result, :sum, rev=true)
    r = first(result, k)
    map!(s -> first(s, 30), r.product, r.product)
    print(r)
    write(filename, repr("text/latex", r))
end