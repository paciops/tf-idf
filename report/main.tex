%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wenneker Assignment
% LaTeX Template
% Version 2.0 (12/1/2019)
%
% This template originates from:
% http://www.LaTeXTemplates.com
%
% Authors:
% Vel (vel@LaTeXTemplates.com)
% Frits Wenneker
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[11pt]{scrartcl} % Font size

\input{structure.tex} % Include the file specifying the document structure and custom commands


%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{	
	\normalfont\normalsize
	\textsc{Università di Pisa}\\ % Your university, school and/or department name(s)
	\vspace{25pt} % Whitespace
	\rule{\linewidth}{0.5pt}\\ % Thin top horizontal rule
	\vspace{20pt} % Whitespace
	{\huge Term Frequency — Inverse Document Frequency}\\ % The assignment title
	\vspace{12pt} % Whitespace
	\rule{\linewidth}{2pt}\\ % Thick bottom horizontal rule
	\vspace{12pt} % Whitespace
}

\author{\LARGE Riccardo Pacioni} % Your name

\date{\normalsize\monthyeardate\today} % Today's date (\today) or a custom date

\begin{document}

\maketitle

\section{Introduzione}
La funzione \textbf{TF-IDF} è una funzione utilizzata spesso in Information
Retrival e in Data Mining, essa infatti definisce quali
parole sono più rilevanti all'interno di un corpus di documenti.
In alternativa può essere utilizzata per evidenziare quali
fra i documenti presenti all'interno della collezione sono più rilevanti
per una data query di ricerca. 

\subsection{Funzionamento}
Questa funzione calcola dei valori per ogni parola presente in un 
documento attraverso 
una proporzione inversa della frequenza di una parola nello 
specifico documento in relazione alla percentuale di documenti 
nella quale quella parola compare.

\subsection{Implicazioni}
Le parole con un alto TF-IDF indicano una forte relazione col documento
in cui compaiono, che a sua volta sarà messo in evidenza in caso queste 
parole compaiano in una query di ricerca.
%quindi se presenti in un'eventuale query di ricerca questo documento risulta essere rilevante.

\section{Il problema}
Lo scopo di questa procedura è ritornare un sub-set di $D$ (chiamato $D^*$)
tale che per ogni $ d \in D^*$ massimizziamo la probabilità
\begin{equation}
	P(d|q,D)
\end{equation}
ovvero di avere il documento $d$ sapendo la query $q$ (fornita dall'utente)
e il set di documenti $D$.

\section{L'algoritmo}
Questa funzione determina la frequenza relativa delle parole in uno specifico 
documento e la compara all'inverso del numero di documenti in cui compaiano.
Quindi calcola quanto un termine è rilevante in un particolare documento.
Parole che compaiono molto frequentemente all'interno di pochi documenti
avranno un valore molto alto, viceversa gli articoli, le congiunzioni e altre
stopwords avranno un valore molto basso in quanto molto frequenti in molti
documenti.
Esistono diversi modi per implementare questa funzione, ma tutti seguono
un'idea comune, calcolata come
\begin{equation}
	tf(t,d) * idf(t,D)
\end{equation}
dove $t$ è un termine, $d$ un documento appartenente a $D$, che a sua volta è
una collezione di documenti. La funzione $tf$ è la funzione che calcola la 
frequenza del termine $t$ in $d$ mentre $idf$ calcola la 
frequenza inversa del termine $t$ all'interno della collezione di documenti.
Quest'ultima è stata implementata nel seguente modo
\begin{equation}
	\label{eq:idf}
	idf(t,D) = \log_{10} \left(\dfrac{N+1}{dt(t,D)+1}\right)
\end{equation}
dove $N$ è la cardinalita di $D$ e $df$ è il numero di documenti in cui compare
il termine $t$.
Per evitare che venga fatta una divisione per zero, al denominatore viene aggiunto 1, mentre
al nominatore viene aggiunto 1 per evitare che il logaritmo ritorni valori negativi.

\section{Test}
Sono stati effettuati tre test con corpora di dimensioni differenti:
\begin{itemize}
	\item il primo con 5 documenti contenenti brevi frasi in inglese;
	\item il secondo con 470 elementi contenenti varie storie in formato
	testuale;
	\item il terzo con 97460 documenti contenenti le descrizioni dei prodotti
	in vendita sul sito di Home Depot.
\end{itemize}
Nei test viene inserita una query e vengono ritornati i $K$ documenti
più rilevanti per quella query.

Per ogni documento $d_i$ viene calcolato il $tf-idf$ di ogni termine
appartenente alla query, la rilevanza di $d_i$ è calcolata come la somma di 
tutti i pesi di quella query
\begin{equation}
	relevance(d_i) = \sum_{j=1}^{n} tf-idf(w_j, d_i, D)
\end{equation}
dove $n$ è il numero di parole presenti nella query e $w_j$ è la parola
alla j-esima posizione.
Infine i $K$ documenti ritornati vengono ordinati in maniera discendente 
rispetto alla colona $sum$.

Il codice della funzione, delle varie funzioni ausiliarie e dei test 
è stato scritto in Julia;
i test hanno provenienza differente:
\begin{itemize}
	\item il corpus più piccolo è stato inventato sul momento;
	\item quello di medie dimensioni proviene 
	dall'\href{http://archives.textfiles.com/stories.zip}{archivio di textfiles.com}
	\cite{medium};
	\item quello di grandi dimensioni proviene dal dataset messo a disposizione 
	da Home Depot su \href{https://www.kaggle.com/c/home-depot-product-search-relevance}{Kaggle}.
\end{itemize}


\section{Limiti}
Questa funzione ha due grosse limitazioni:
\begin{enumerate}
	\item sinonimi o plurali non vengono riconosciuti da questa procedura, 
	infatti parole come ``padre'' risulta essere differente da 
	``padri'' e da ``papà'; per ovviare a questo problema potrebbe essere 
	d'aiuto effettuare una lemmatizzazione\footnote{La lemmatizzazione è il 
	processo di riduzione di una forma flessa di una parola alla sua forma 
	canonica (non marcata), detta lemma. 
	Nell'elaborazione del linguaggio naturale, la lemmatizzazione è il 
	processo algoritmico che determina automaticamente il lemma di
	 una data parola.\cite{lemming}} e/o uno Stemming\footnote{Lo stemming 
	è il processo di riduzione della forma flessa di una parola alla sua
	 forma radice, detta "tema". Il tema non corrisponde necessariamente 
	 alla radice morfologica (lemma) della parola: normalmente è 
	 sufficiente che le parole correlate siano mappate allo stesso tema, 
	 anche se quest'ultimo non è una valida radice per la parola.\cite{stemming}};
	\item la seconda è che l'ordine delle parole nella query non viene preso
	in considerazione e se fosse rilevante i documenti importanti non sarebbero
	presi in considerazione.
\end{enumerate}

\section{Risultati}
Nella tabella \ref{tab:small} possiamo vedere come usando la query ``buy car'' il primo
documento (ossia quello con il valore $sum$ più alto) contenga entrambe le 
parole ``buy'' e ``car''. Il secondo ed il terzo invece contengono solo uno
dei due termini. In questo caso il secondo documento ha un valore più alto
in quanto ``buy'' è presente in meno documenti rispetto a ``car''.
Sulla colonna $corpus$ troviamo il contenuto di ogni documento.

\begin{table}[ht]
	\centering
	\input{table_small.tex}
	\caption{Risultati sul corpus di documenti piccolo.\label{tab:small}}
\end{table}

Nella tabella \ref{tab:med} invece vengono elencati solo i 10 documenti più rilevanti
rispetto alla query ``the monkey king'' (a differenza della precedente tabella vengono
riportati i path dei file e non il contenuto in quanto troppo lungo).

\begin{table}[ht]
	\centering
	\input{table_medium.tex}
	\caption{Risultati sul corpus di documenti medio.\label{tab:med}}
\end{table}

Nella tabella \ref{tab:big} abbiamo i 10 prodotti più rilevanti per la query 
``black home decorators collections'', la rilevanza dei termini viene calcolata sulla
descrizione dei prodotti, mentre sull'ultima colonna sono presenti i nomi dei prodotti,
troncati a 30 caratteri per motivi di spazio.

\begin{table}
	\centering
	\input{table_big.tex}
	\caption{Risultati sul corpus di documenti grande.\label{tab:big}}
\end{table}

\section{Appendice}
\label{sec:end}

\subsection{Funzione e sottofunzioni}
Innanzitutto nel codice la funzione $tf-idf$ viene separata
in due sottofunzioni:
\begin{itemize}
	\item DF, ovvero $Document$ $Frequency$, che per ogni parola presente nel corpus
	conta in quanti documenti essa è presente;
	\item TF, ovvero $Term$ $Frequency$, che conta quante volte un termine $t$
	è presente in un documento $d$ e lo divide per la cardinalità di $d$.
\end{itemize}

\subsection{Document Frequency}
\label{sec:df}
Questa funzione prende in input una collezione di documenti, passata come un
array di stringhe e ritorna un dizionario dove la chiave è un termine presente
nel corpus, mentre il valore è il numero di documenti che contengono quel valore.
Per eseguire queste operazioni innanzitutto viene creato un dizionario di appoggio
che avrà come chiave sempre un termine del corpus, ma come valore un insieme, che
andrà a contenere l'indice del documento che contiene quel termine.
Successivamente viene effettuato un ciclo su ogni documento
del corpus, dentro al ciclo si itera su ogni parola presente nel documento:
a questo punto se la parola non è presente all'interno del dizionario, si crea 
l'insieme con all'interno l'indice del documento su cui sto ciclando;
se la parola è già presente, si aggiunge l'indice del documento.
Avendo scelto gli insiemi e non una lista abbiamo la garanzia che non ci possano
essere duplicati e che quindi un documento non venga contato più di una volta.
Infine il dizionario, prima di essere ritornato, viene modificato tramite una $map$:
si cicla su ogni valore del dizionario e ogni insieme viene sostituito con la
sua cardinalità.
In questo modo si calcola quali sono i termini più frequenti all'interno del corpus.
\subsection{Term Frequency}
Questa funzione è ben più semplice della precedente: i parametri passati sono un
termine ed un documento (entrambi codificati come stringhe), il documento viene
trasformato in una lista di stringhe, vengono contate tutte le occorrenze del
termine all'interno di questa lista ed il numero ottenuto, prima di essere 
ritornato, viene diviso per la lunghezza della lista.
In questo modo si calcola la frequenza di un termine all'interno di un documento.

\subsection{La funzione TF-IDF}
La funzione vera e propria prende in input una query ed un corpus di documenti,
codificati rispettivamente con una stringa e con un array di stringhe.
Verrà ritornata una matrice simile alle tabelle \ref{tab:small}, \ref{tab:med} e
\ref{tab:big}, dove sulle righe troviamo i documenti appartenenti al corpus e sulle
colonne i termini appartenenti alla query.
I valori contenuti ovviamente sono il $td-idf$ applicato al termine $t_i$ e al 
documento $d_j$, dove $i$ è la colonna i-esima e $j$ è la riga j-esima.
Come prima cosa, in questa funzione la query viene trasformata in una lista di
stringhe, che d'ora in poi verrà chiamata ``termini'', poi viene fatto del 
preprocessing sul corpus di documenti:
\begin{enumerate}
	\item vengono eliminati tutti i caratteri non ASCII;
	\item tutte le parole appartenenti al corpus e alla query vengono 
	messe in minuscolo, in modo che i documenti e la query siano case-insensitive.
\end{enumerate}
Successivamente viene calcolata la $Document$ $Frequency$ (vedi sezione \ref{sec:df}) 
e viene salvata in un dizionario, poi viene calcolata la dimensione dell'array
che contiene il corpus di documenti.
Viene inizializzata la matrice come un array di array di Float64, la scelta del tipo
Float64 è dovuta al fatto che tutti i valori sono decimali fra 0 e 1 e altri tipi come 
Int o UInt non avrebbero potuto memorizzare la parte decimale.
A questo punto prima si cicla su tutti i termini e per ogni termine si cicla sui documenti:
\begin{itemize}
	\item tramite una funzione ausiliaria $get\_freq$\footnote{Questa funzione 
	ha lo scopo di ritornare 0 in caso il termine non sia presente nel dizionario, 
	che altrimenti ritornerebbe un errore.} si ottiene la frequenza 
	del termine corrente nel dizionario generato dalla funzione $DF$;
	\item a questo punto si calcola la frequenza inversa dei documenti ($IDF$) seguendo
	l'equazione \ref{eq:idf};
	\item infine il valore calcolato viene inserito in un array temporaneo, che a sua
	volta verrà inserito come colonna della matrice.
\end{itemize}
La scelta di utilizzare una matrice come struttura dati che contiene i valori
è dovuta al fatto che semplifica molto la visualizzazione dei dati ed è 
facilmente trasformabile in un DataFrame.
Nei file di test infatti, i DataFrame sono creati a partire dalla matrice e sono stati
utilizzati in quanto permettono di manipolare facilmente i dati, in questo caso
semplificano la creazione di una colonna a partire dai dati già esistenti e 
permettono di riordinare l'intero dataset specificando solo una colonna.

\lstinputlisting[
	style=code, 
	caption={Il file tfidf.jl,  che contiene la funzione principale e le funzioni ausiliarie}
	]{../tfidf.jl}
\lstinputlisting[
	style=code, 
	caption={Il file di test che esegue la funzione su un corpus di piccole dimensioni.}
]{../tfidf_test.jl}
\lstinputlisting[
	style=code, 
	caption={Il file di test che esegue la funzione su un corpus di medie dimensioni.}
]{../tfidf_test_medium.jl}
\lstinputlisting[
	style=code, 
	caption={Il file di test che esegue la funzione su un corpus di grandi dimensioni.}
]{../tfidf_test_big.jl}

\begin{thebibliography}{6}
	\bibitem{}
	Manning, Christopher D and Raghavan, Prabhakar and Schütze, Hinrich,
	\emph{Introduction to information retrieval},
	Cambridge University Press, 2008

	\bibitem{medium}
	William Scott: \emph{TF-IDF from scratch in python on real world dataset},
	\\\texttt{https://towardsdatascience.com/tf-idf-for-document-ranking-
	from-scratch-in-python-on-real-world-dataset-796d339a4089}

	\bibitem{}
	Ramos, Juan. 
	"Using tf-idf to determine word relevance in document queries." 
	\emph{Proceedings of the first instructional conference on machine learning}
	Vol. 242. 2003.

	\bibitem{}
	Wikipedia contributors. "Tf–idf." Wikipedia, The Free Encyclopedia. Wikipedia, The Free Encyclopedia, 11 Oct. 2020. Web. 15 Oct. 2020. 

	\bibitem{stemming}
	Wikipedia contributors. "Stemming." Wikipedia, The Free Encyclopedia. Wikipedia, The Free Encyclopedia, 7 Oct. 2020. Web. 16 Oct. 2020. 

	\bibitem{lemming}
	Wikipedia contributors. "Lemmatisation." Wikipedia, The Free Encyclopedia. Wikipedia, The Free Encyclopedia, 12 Oct. 2020. Web. 16 Oct. 2020. 

\end{thebibliography}
\end{document}

