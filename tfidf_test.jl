using DataFrames
include("tfidf.jl")

filename = "report/table_small.tex"
query = "buy car"
corpus = [
    "The car is driven on the road",
    "The truck is driven on the highway",
    "I need to buy a new car",
    "He takes the bike to go to school",
    "The price for this car is pretty expensive",
    "Did you go to buy a new table?"
]
@time begin
    result = DataFrame(tf_idf(query, corpus))
    rename!(result, Symbol.(split(query)))
    result.sum = sum(eachcol(result))
    result.corpus = corpus
    sort!(result, :sum, rev=true)
    println(result)
    write(filename, repr("text/latex", result))
end