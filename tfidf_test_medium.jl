using DataFrames
include("tfidf.jl")

arguments = ARGS
k = 10
filename = "report/table_medium.tex"

@time begin
    # read all files
    documents = String[]
    paths = String[]
    @time begin
        println("start reaing files")
        for (root, dirs, files) in walkdir("stories/")
            for file in files
                if first(file) != '.'
                    path = joinpath(root, file)
                    push!(paths, path)
                    content = open(path, "r") 
                    push!(documents, read(content, String))
                    close(content) 
                end
            end 
        end
        println("end reaing files")
    end
    matrix = tf_idf(join(arguments, " "),  documents)
    result = DataFrame(matrix)
    rename!(result, Symbol.(arguments))
    result.sum = sum(eachcol(result))
    result.path = paths
    sort!(result, :sum, rev=true)
    print(first(result, k))
    write(filename, repr("text/latex", first(result, k)))
end