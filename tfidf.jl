using ProgressMeter

function df(documents::Array{String})::Dict{String,Number}
    DF = Dict{String,Any}()
    @showprogress "DF " for (index, sentence) in enumerate(documents)
        for word in split(sentence)
            try
                push!(DF[word], index)
            catch
                DF[word] = Set(index)
            end
        end
    end
    map!(v -> length(v), values(DF))
    return DF
end

function tf(term::String, document::String)::Number
    words = split(document)
    return count(w -> w == term, words) / length(words)
end

function get_freq(dict::Dict{String,Number}, key::String)::Number
    try
        return dict[key]
    catch LoadError
        return 0
    end
end

function tf_idf(query::String, documents::Array{String})::Array{Array{Float64}}
    terms::Array{String} = split(lowercase(query))
    map!(s -> filter(isascii, s), documents, documents)
    map!(lowercase, documents, documents)
    DF = df(documents)
    N = length(documents)
    # for each document
    result = Array{Float64}[]
    for term in terms
        println("Term:\t", term)
        tmp = Float64[]
        @showprogress "TF-IDF " for (index, doc) in enumerate(documents)
            doc_freq = get_freq(DF, term)
            # numerator is added 1 to avoid negative values
            idf = log10((N + 1) / (doc_freq + 1)) 
            push!(tmp, tf(term, doc) * idf)
        end
        push!(result, tmp)
    end
    return result
end